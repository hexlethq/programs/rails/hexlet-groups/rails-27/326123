# frozen_string_literal: true

# rubocop:disable Style/For
def build_query_string(hash)
  arr = []
  hash.sort.each do |key, value|
    arr << "#{key}=#{value}"
  end

  arr.join('&')
end
# rubocop:enable Style/For
