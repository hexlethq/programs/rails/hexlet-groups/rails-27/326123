# frozen_string_literal: true

def compare_versions(version1, version2)
  version1 = version1.split(".")
  version2 = version2.split(".")
  if version1[0].to_i > version2[0].to_i
    return 1
  elsif version1[0].to_i == version2[0].to_i
    if version1[1].to_i > version2[1].to_i
      return 1
    elsif version1[1].to_i == version2[1].to_i
      return 0
    else
      return -1
    end
  else
    return -1
  end
end
