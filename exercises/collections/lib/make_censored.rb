# frozen_string_literal: true

# rubocop:disable Style/For

def make_censored(text, stop_words)
  res = []
  text.split(" ").each do |word|
    word = '$#%!' if stop_words.include? word
    res << word
  end
  res.join(' ')
end
# rubocop:enable Style/For
