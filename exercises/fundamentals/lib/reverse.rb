# frozen_string_literal: true

def reverse(string)
  result = +''
  i = 0
  # size = 0

  # until 'me' == 'programer'
  #   !string[size].nil? ? size += 1 : break
  # end

  # while i < size
  #   result[i] = string[size - i - 1]
  #   i += 1
  # end

  until string[i] == nil
    i += 1
    result += string.chars[-i]
  end

  result
end

puts reverse('Hexlet')
