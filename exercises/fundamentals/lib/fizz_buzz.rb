# frozen_string_literal: true

def fizz_buzz(start, stop)
  arr = []

  if stop >= start
    while stop >= start
      if start % 3 == 0 && start % 5 == 0
        arr << 'FizzBuzz'
      elsif start % 3 == 0
        arr << 'Fizz'
      elsif start % 5 == 0
        arr << 'Buzz'
      else
        arr << start.to_s
      end
      start += 1
    end
  end

  arr.join(' ')
end
