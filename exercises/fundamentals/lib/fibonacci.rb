# frozen_string_literal: true

def fibonacci(n)
  if n == 0 || n == 1
    return n
  elsif n < 0
    return nil
  else n > 1
    return fibonacci(n - 1) + fibonacci(n - 2)
  end
end
