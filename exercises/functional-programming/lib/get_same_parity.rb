# frozen_string_literal: true

def get_same_parity(string)
  res = []
  i = 0

  if !string[0].nil?
    if string[0].odd?
      while !string[i].nil?
        res << string[i] if string[i].odd?
        i += 1
      end
    elsif string[0].even?
      while !string[i].nil?
        res << string[i] if string[i].even?
        i += 1
      end
    end
    res
  else
    string
  end
end
