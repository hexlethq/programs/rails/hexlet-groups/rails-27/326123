# frozen_string_literal: true

def count_by_years(users)
  users.each_with_object(arr = []) { |hash| arr << hash[:birthday].split('-').first if hash.value? 'male'}
  arr.each_with_object(res = {}) { |i| res[i] = arr.count(i) }
  res
end
