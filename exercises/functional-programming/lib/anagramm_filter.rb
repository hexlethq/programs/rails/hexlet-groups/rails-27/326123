# frozen_string_literal: true

def anagramm_filter(word, arr)
  return arr if arr == []

  arr.each_with_object(res = []) { |i| res << i if i.chars.sort.uniq == word.chars.sort.uniq }
  res
end
